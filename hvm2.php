<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>HVM - Property & Real Estate</title>
    <!-- Favicon icon -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
    <link rel="stylesheet" href="./vendor/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="./vendor/magnific-popup/magnific-popup.min.css">
    <link rel="stylesheet" href="./vendor/lightgallery/css/lightgallery.min.css">
    <link rel="stylesheet" href="./vendor/animate/animate.css">
	<!-- Custom Stylesheet -->
	<link rel="stylesheet" href="./css/circle.css">
    <link rel="stylesheet" href="./css/style.css">
	<!-- REVOLUTION SLIDER CSS -->
    <link rel="stylesheet" type="text/css" href="./vendor/revolution/revolution/css/revolution.min.css">
    <style>
        .site-header{
            position:fixed;
            width:100%;
        }
		#container-kpr{
			padding:5%;
			background:#f5d393;
			border-radius:15px;
			
		}
		#container-kpr .form-group{
			margin-bottom:8%;
		}
		#container-hasil-kpr h2{
			text-align:center;
			color:black;
		}
		#container-hasil-kpr{
			color:black;
			padding:5%;
		}
		#container-kpr h2{
			text-align:center;
		}
		#kalkulator-kpr label{
			color:black;
		}

        #sticky-noted{
            position:absolute;
             z-index:9999999999; 
             right:15%;
             bottom:-20%;
            width:277px; 
            background :hsla(180,4%,95%,.8);
            padding:33px;
        }
        #sticky-noted h1{
            font-size: 36px;
            margin: .75em 0;
            line-height: 1.44;
            font-weight: 300;
        }
        #glasses-parallax{
            position:absolute;
            top:0;
            left:50%;
            transform: translate(-50%, -50%);
		}
		@media only screen and (max-width: 600px) {
			#glasses-parallax{
					width:26%;
					
			}
			#sticky-noted{
				width:30%;
				right:23%;
				padding:2%;
				bottom:-21%;
			}
			#sticky-noted img{
				width:80%;
			}
			#sticky-noted h1{
				font-size:11px;
			}
			.contentCircle{
				width: 50%;
				height: 50%;
				border-radius: 100%;
				background: url(../img/bgcircle.png) no-repeat;
				color: #fff;
				position: relative;
				top: 24%;
				right: 0;
				bottom: 0;
				left: 0;
				box-shadow: 0px 0px 69px 1px #2b152e;
				margin: auto;
			}
			#text-after-noteds{
				margin-top:15%;
			}


		}
		.owl-nav{
	display:none;
	
}
	#vertical-button{
		position: fixed;
		bottom: 32%;
		left: -8%;
		background: black;
		color:white;
		padding:1%;
		/* transform-origin: 0 0; */
		transform: rotate(269deg);
		z-index:99999999;
	}
	#vertical-button .display-6{
		font-size:1.9rem;
	}
    </style>
</head>
<body id="bg">
<div id="vertical-button">
	<span class="display-6">Simulate Moortage</span>
</div>	
<div class="page-wraper">
<div id="loading-area"></div>
	<!-- header -->
	<header class="site-header" id="navbar-hvm">
		<!-- main header -->
		<div class="sticky-header main-bar-wraper navbar-expand-lg">
			<div class="main-bar clearfix ">
				<div class="container-fluid clearfix">
					<!-- website logo -->
					<div class="logo-header mostion logo-dark">
						<a href="index.html" ><img id="logo-hvm" src="images/logo.png" alt=""></a>
					</div>
					<!-- nav toggle button -->
					<button class="navbar-toggler collapsed navicon justify-content-end" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
						<span></span>
						<span></span>
						<span></span>
					</button>
					<!-- extra nav -->
					<div class="header-nav navbar-collapse collapse justify-content-end" id="navbarNavDropdown">
						<div class="logo-header d-md-block d-lg-none">
							<a href="index.html"><img src="images/logo.png" alt=""></a>
						</div>
						<ul class="nav navbar-nav navbar">	
							<li class="active"><a href="javascript:void(0);">Home<i class="fa fa-chevron-down"></i></a>
								<ul class="sub-menu">
									<li><a href="index.html">Home 1</a></li>
									<li><a href="index-2.html">Home 2</a></li>
									<li><a href="index-3.html">Home 3</a></li>
									<li><a href="index-4.html">Landing Page</a></li>
								</ul>
							</li>
							<li class="active"><a href="javascript:void(0);">Pages<i class="fa fa-chevron-down"></i></a>
								<ul class="sub-menu">
									<li><a href="about-us-1.html">About Us 1</a></li>
									<li><a href="about-us-2.html">About Us 2</a></li>
									<li><a href="company-exhibition.html">Company Exhibition</a></li>
									<li><a href="price-table.html">Price Table</a></li>
									<li><a href="company-history.html">Company History</a></li>
									<li><a href="privacy-policy.html">Privacy Policy</a></li>
									<li><a href="404-page.html">404 Error</a></li>
									<li><a href="coming-soon.html">Coming Soon</a></li>
								</ul>
							</li>
							<li><a href="javascript:void(0);">Portfolio<i class="fa fa-chevron-down"></i></a>
								<ul class="sub-menu">
									<li><a href="portfolio-1.html">Portfolio 1</a></li>
									<li><a href="portfolio-2.html">Portfolio 2</a></li>
									<li><a href="project-detail-1.html">Project Detail 1</a></li>
									<li><a href="project-detail-2.html">Project Detail 2</a></li>
									<li><a href="project-detail-3.html">Project Detail 3</a></li>
									<li><a href="project-detail-4.html">Project Detail 4</a></li>
								</ul>
							</li>
							<li><a href="javascript:void(0);">Blog<i class="fa fa-chevron-down"></i></a>
								<ul class="sub-menu">
									<li><a href="blog-grid.html">Blog Grid</a></li>
									<li><a href="blog-list.html">Blog List</a></li>
									<li><a href="blog-masonry.html">Blog Masonry</a></li>
									<li><a href="blog-details.html">Blog Details</a></li>
								</ul>
							</li>
							<li><a href="javascript:void(0);">Contact Us<i class="fa fa-chevron-down"></i></a>
								<ul class="sub-menu">
									<li><a href="contact-us-1.html">Contact Us 1</a></li>
									<li><a href="contact-us-2.html">Contact Us 2</a></li>
								</ul>
							</li>
						</ul>
						<div class="dlab-social-icon">
							<ul>
								<li><a class="site-button circle fa fa-facebook" href="javascript:void(0);"></a></li>
								<li><a class="site-button  circle fa fa-twitter" href="javascript:void(0);"></a></li>
								<li><a class="site-button circle fa fa-linkedin" href="javascript:void(0);"></a></li>
								<li><a class="site-button circle fa fa-instagram" href="javascript:void(0);"></a></li>
							</ul>
						</div>	
						
					</div>
				</div>
			</div>
		</div>
		<!-- main header END -->
	</header>
	<!-- header END -->
	
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- Slider -->
        <div class="main-slider style-two default-banner" id="home">
            <div class="tp-banner-container">
                <div class="tp-banner" >
					<div id="rev_slider_1077_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="scroll-effect136" data-source="gallery" style="background-color:#111111;padding:0px;">
						<!-- START REVOLUTION SLIDER 5.3.0.2 fullscreen mode -->
						<div id="rev_slider_1077_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.3.0.2">
							<ul>	
								<!-- SLIDE  -->
								<li data-index="rs-3042" data-transition="slideoverhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1000"  data-thumb="images/main-slider/slide3.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="MODERN HOME" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
									<!-- MAIN IMAGE -->
									<img src="images/main-slider/slide3.jpg" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="110" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 200" data-offsetend="0 -200" alt="" data-bgparallax="10" class="rev-slidebg" data-no-retina="">
									<div class="tp-caption tp-shape tp-shapewrapper " id="slide-100-layer" 
										data-x="['center','center','center','center']" 
										data-hoffset="['0','0','0','0']" 
										data-y="['middle','middle','middle','middle']" 
										data-voffset="['0','0','0','0']" 
										data-width="full" data-height="full" 
										data-whitespace="nowrap" 
										data-type="shape" 
										data-basealign="slide" 
										data-responsive_offset="off" 
										data-responsive="off" 
										data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}]' 
										data-textAlign="['left','left','left','left']" 
										data-paddingtop="[0,0,0,0]" 
										data-paddingright="[0,0,0,0]" 
										data-paddingbottom="[0,0,0,0]" 
										data-paddingleft="[0,0,0,0]" 
										style="z-index: 2;background-color:rgba(0, 0, 0, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>
									
								</li>
								<!-- SLIDE  -->
								<li data-index="rs-3043" data-transition="slideoverhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1000"  data-thumb="images/main-slider/slide4.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="APARTMENTS" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
									<!-- MAIN IMAGE -->
									<img src="images/main-slider/slide4.jpg" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="110" data-rotatestart="0" data-rotateend="0" data-offsetstart="-500 0" data-offsetend="500 0" alt="" data-bgparallax="10" class="rev-slidebg" data-no-retina="">
									<div class="tp-caption tp-shape tp-shapewrapper " id="slide-200-layer" 
										data-x="['center','center','center','center']" 
										data-hoffset="['0','0','0','0']" 
										data-y="['middle','middle','middle','middle']" 
										data-voffset="['0','0','0','0']" 
										data-width="full" data-height="full" 
										data-whitespace="nowrap" 
										data-type="shape" 
										data-basealign="slide" 
										data-responsive_offset="off" 
										data-responsive="off" 
										data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}]' 
										data-textAlign="['left','left','left','left']" 
										data-paddingtop="[0,0,0,0]" 
										data-paddingright="[0,0,0,0]" 
										data-paddingbottom="[0,0,0,0]" 
										data-paddingleft="[0,0,0,0]" 
										style="z-index: 2;background-color:rgba(0, 0, 0, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>
								</li>
								<!-- SLIDE  -->
							</ul>
							<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	
						</div>
					</div>    
				</div>        
			</div>        
		</div>        
		<!-- Slider END -->
		<div class="content-block">
			<!-- About Me -->
			<section class="content-inner exhibition-bx">
				<div class="container">
					<div class="row align-items-center" >
                        <div class="col-sm-12 fadeIn">
                            <h1 class="text-center">DISCOVER THE NEW</h1>
                            <img src="images/logo.png" class="d-block" style="margin:0 auto;" alt="">
                        </div>
						<div class="col-lg-12 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
							<div class="exhibition-carousel owl-carousel">
								<div class="item"><img src="https://via.placeholder.com/1200x600" alt=""></div>
                                <div class="item"><img src="https://via.placeholder.com/1200x600" alt=""></div>
                                
							</div>
                            <div id="sticky-noted">
                                <img style="width:50%; display:block; margin:0 auto;" src="https://theresortliving.com/content/themes/gj-boilerplate/img/opening-quotes-icon.svg?x89339" alt="Opening quotes">
                                <h1>Lorem ipsum dolor sit amet consectetur adipisicing elit.</h1>
                            </div>
						</div>
                        <div class="col-sm-6" id="text-after-noteds">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur est repellat placeat architecto iste, fugit dolorum quam itaque modi, autem, corporis pariatur neque id delectus ducimus eveniet. Nisi, reprehenderit alias?</p>
                        </div>
					</div>
				</div>
			</section>
            <!-- About Us -->

            <section class="content-inner-2 project-area" style="background:#c9e5f3; background-size: cover;">
                <div class="container">
                    <img class="img-fluid" id="glasses-parallax"src="https://theresortliving.com/content/themes/gj-boilerplate/img/glasses.png?x89339">
                    <div class="section-head text-white text-center">
                        <h2 class="title text-white">The Luxury Residence In Forest</h2>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolore obcaecati totam nobis repudiandae eum possimus mollitia quod, in aut exercitationem quasi minima aliquid veniam animi expedita, consequatur autem sapiente nisi.</p>
					</div>
                    <div class="row spno service-row">
						<div class="col-sm-4 wow bounceInUp" data-wow-duration="2s" data-wow-delay="0.2s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.2s; animation-name: bounceInUp;">
							<div class="service-box">
								<div class="media">
									<img src="images/services/pic1.jpg" alt="">
								</div>
								<div class="info">
									<h4 class="title">24 HRS. Water Supply</h4>
									<p>The KingArchitect is a collection of grand proportioned.</p>
								</div>
							</div>
						</div>
						<div class="col-sm-4 wow bounceInUp" data-wow-duration="2s" data-wow-delay="0.4s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.4s; animation-name: bounceInUp;">
							<div class="service-box">
								<div class="media">
									<img src="images/services/pic2.jpg" alt="">
								</div>
								<div class="info">
									<h4 class="title">CCTV With Intercom</h4>
									<p>The KingArchitect is a collection of grand proportioned.</p>
								</div>
							</div>
						</div>
						<div class="col-sm-4 wow bounceInUp" data-wow-duration="2s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.6s; animation-name: bounceInUp;">
							<div class="service-box">
								<div class="media">
									<img src="images/services/pic3.jpg" alt="">
								</div>
								<div class="info">
									<h4 class="title">Power Backup</h4>
									<p>The KingArchitect is a collection of grand proportioned.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>	

			<section data-content="USP" class="mt-5">

				<div class="container">
					<div class="row">
					<div class="holderCircle">

					<div class="dotCircle">
						<span class="itemDot active itemDot1" data-tab="1">
							<i class="fa fa-life-ring"></i>
							<span class="forActive"></span>
						</span>
						<span class="itemDot itemDot2" data-tab="2">
							<i class="fa fa-bomb"></i>
							<span class="forActive"></span>
						</span>
						<span class="itemDot itemDot3" data-tab="3">
							<i class="fa fa-heartbeat"></i>
							<span class="forActive"></span>
						</span>
						<span class="itemDot itemDot4" data-tab="4">
							<i class="fa fa-leaf"></i>
							<span class="forActive"></span>
						</span>
						<span class="itemDot itemDot5" data-tab="5">
							<i class="fa fa-magic"></i>
							<span class="forActive"></span>
						</span>
						<span class="itemDot itemDot6" data-tab="6">
							<i class="fa fa-pencil-square-o"></i>
							<span class="forActive"></span>
						</span>
						<span class="itemDot itemDot7" data-tab="7">
							<i class="fa fa-rss-square"></i>
							<span class="forActive"></span>
						</span>
					</div>

					<div class="contentCircle">

						<div class="CirItem active CirItem1">
							TEXT SAMPLE FOR ITEM 1
						</div>
						<div class="CirItem CirItem2">
							TEXT SAMPLE FOR ITEM 2
						</div>
						<div class="CirItem CirItem3">
							TEXT SAMPLE FOR ITEM 3
						</div>
						<div class="CirItem CirItem4">
							TEXT SAMPLE FOR ITEM 4
						</div>
						<div class="CirItem CirItem5">
							TEXT SAMPLE FOR ITEM 5
						</div>
						<div class="CirItem CirItem6">
							TEXT SAMPLE FOR ITEM 6
						</div>
						<div class="CirItem CirItem7">
							TEXT SAMPLE FOR ITEM 7
						</div>
					</div>

</div>


					</div>
				</div>
			</section>



			<!-- Video Section -->
			<section data-content="Calculator Mortage" class="p-5">
				<div class="container">
			
        	<div class="row ">
            <div class="col-xs-12 col-sm-12 col-lg-6 col-md-6" id="container-kpr">
				<h2 class="wlcm-form-title mb-5">Kalkulator KPR</h2>
                <form id="kalkulator-kpr" method="post" onsubmit="return process()">
                    <div class="form-group">
                        <label for="harga_rumah">Harga Rumah (dalam Rp)</label>
                        <input type="number" name="harga_rumah" min="50000000" id="harga_rumah" class="form-control input-sm" required="required" autofocus placeholder="Contoh : 300000000" value="300000000" />
                    </div>
                    <div class="form-group">
                        <label for="uang_muka">Uang Muka (minimal 20% dari harga rumah, dalam Rp)</label>
                        <input type="number" name="uang_muka" min="1" id="uang_muka" class="form-control input-sm" required="required" placeholder="Contoh : 60000000" value="60000000" />
                    </div>
                    <div class="form-group">
                        <label for="margin">Margin (dalam persen disetarakan p.a, asumsi kisaran 7.00 - 9.50)</label>
                        <input type="number" name="margin" min="5" max="20" id="margin" class="form-control input-sm" required="required" step="any" placeholder="Contoh : 8.50" value="8"/>
                    </div>
                    <div class="form-group">
                        <label for="tenor">Jangka Waktu (dalam tahun)</label>
                        <select name="tenor" id="tenor" class="form-control input-sm">
                            <option value="5">5 tahun</option>
                            <option value="10">10 tahun</option>
                            <option value="15" selected>15 tahun</option>
                            <option value="20">20 tahun</option>
                            <option value="25">25 tahun</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="penghasilan">Total Penghasilan Bulanan (gabungan suami istri, dalam Rp)</label>
                        <input type="number" name="penghasilan" id="penghasilan" class="form-control input-sm" required="required" placeholder="Contoh : 10000000" value="10000000"/>
                    </div>
                    <div class="form-group">
                        <label for="cicilan_lain">Cicilan Lain per Bulan (bila ada (misal kredit mobil atau KPR), dalam Rp)</label>
                        <input type="number" name="cicilan_lain" id="cicilan_lain" class="form-control input-sm" placeholder="Contoh : 1500000" value="0"/>
                    </div>
                    <button type="submit" class="btn btn-primary btn-md col-xs-12">HITUNG CICILAN</button>
                </form>
            </div>
            <div class="col-xs-12 col-sm-12 col-lg-6 col-md-6" id="container-hasil-kpr">
                <h2 class="mb-5">Hasil Perhitungan</h2>
                <table class="table table-borderless" style="color:black;">
                    <tbody>
						<tr style="background:#F2F2F2;">
							<td rowspan="3" valign="top">Pinjaman </td>
							<td> = Harga Rumah - Uang Muka</td>
						</tr>
						<tr style="background:#F2F2F2">
							<td> = Rp <span id="hasil_harga_rumah">0</span> - Rp <span id="hasil_uang_muka">0</span></td>
						</tr>
						<tr style="background:#F2F2F2">
							<td> = Rp <strong><span id="pinjaman">0</span></strong></td>
						</tr>
						<tr>
							<td rowspan="3" valign="top">Total Pinjaman</td>
							<td> = Pinjaman + (Pinjaman * Margin * Tenor)</td>
						</tr>
                        <tr>
                            <td> = Rp <span class="hasil_pinjaman">0</span> + (Rp <span class="hasil_pinjaman">0</span> * <span id="hasil_margin">0</span>% * <span class="hasil_tenor">0</span> tahun)</td>
                        </tr>
                        <tr>
                            <td> = Rp <strong><span id="total_pinjaman">0</span></strong></td>
                        </tr>
                        <tr style="background:#F2F2F2">
                            <td rowspan="3" valign="top">Cicilan / bulan </td>
                            <td> = Total Pinjaman / Tenor / 12 bulan</td>
                        </tr>
                        <tr style="background:#F2F2F2">
                            <td> = Rp <span id="hasil_total_pinjaman">0</span> / <span class="hasil_tenor">0</span> / 12 </td>
                        </tr>
                        <tr style="background:#F2F2F2">
                            <td> = Rp <strong><span id="cicilan_bulanan">0</span></strong></td>
                        </tr>
                        <tr>
                            <td rowspan="2" valign="top">Persentase Cicilan</td>
                            <td> = Cicilan Bulanan / Penghasilan Bulanan</td>
                        </tr>
                        <tr>
                            <td> = <strong><span id="persentase_cicilan">0</span> %</strong></td>
                        </tr>
                        <tr style="background:#F2F2F2">
                            <td colspan="2">Pengajuan KPR kemungkinan besar diterima bila persentase cicilan &lt;= 40 persen</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
				</div>
			</section>
			<!-- Video Section END -->
           
			
			<!-- Our Team -->
			<section class="content-inner-2 mb-5" data-content="FACILITY">
				<div class="container">
					<div class="section-head text-center">
						<!-- <p>Meet The Team </p> -->
						<h2 class="title">FACILITY</h2>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-6 col-sm-6 m-md-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
							<div class="our-team team-style1">
								<div class="dlab-media radius-sm">
									<img src="images/team/our-team/pic1.jpg" alt="">
								</div>
								<div class="team-title-bx text-center">
									
									<span class="clearfix"></span>
									<h2 class="team-title">GYM</h2>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6 m-md-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.4s">
							<div class="our-team team-style1">
								<div class="dlab-media radius-sm">
									<img src="images/team/our-team/pic2.jpg" alt="">
								</div>
								<div class="team-title-bx text-center">
									<h2 class="team-title">Pham Zima</h2>
									<span class="clearfix">REALTOR</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6 m-sm-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.6s">
							<div class="our-team team-style1">
								<div class="dlab-media radius-sm">
									<img src="images/team/our-team/pic3.jpg" alt="">
								</div>
								<div class="team-title-bx text-center">
									<h2 class="team-title">Philip Demarco</h2>
									<span class="clearfix">ARCHITECT</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.8s">
							<div class="our-team team-style1">
								<div class="dlab-media radius-sm">
									<img src="images/team/our-team/pic4.jpg" alt="">
								</div>
								<div class="team-title-bx  text-center">
									<h2 class="team-title">Philip Demarco</h2>
									<span class="clearfix">MANAGER</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Our Team END -->
		</div>
    </div>
	<!-- Content END-->
	
	<!-- Footer -->
    <footer class="site-footer" id="footer">
        <!-- footer bottom part -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-md-left text-center"> <span>© 2021 Cendana Icon. All Right Reserved</span> </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer END-->

</div>


<!-- JAVASCRIPT FILES ========================================= -->
<script src="js/jquery.min.js"></script><!-- JQUERY.MIN JS -->
<script src="vendor/wow/wow.js"></script><!-- WOW JS -->
<script src="vendor/bootstrap/js/popper.min.js"></script><!-- POPPER.MIN JS -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->
<script src="vendor/owl-carousel/owl.carousel.js"></script><!-- OWL-CAROUSEL JS -->
<script src="vendor/magnific-popup/magnific-popup.js"></script><!-- MAGNIFIC-POPUP JS -->
<script src="vendor/counter/waypoints-min.js"></script><!-- WAYPOINTS JS -->
<script src="vendor/counter/counterup.min.js"></script><!-- COUNTERUP JS -->
<script src="vendor/imagesloaded/imagesloaded.js"></script><!-- IMAGESLOADED -->
<script src="vendor/masonry/masonry-3.1.4.js"></script><!-- MASONRY -->
<script src="vendor/masonry/masonry.filter.js"></script><!-- MASONRY -->
<script src="vendor/lightgallery/js/lightgallery-all.min.js"></script><!-- LIGHTGALLERY -->
<script src="js/dz.carousel.js?v=2"></script><!-- OWL-CAROUSEL -->
<script src="js/custom.js"></script><!-- CUSTOM JS -->
<script src="js/circle.js"></script>

<!-- REVOLUTION JS FILES -->
<script src="vendor/revolution/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/revolution/revolution/js/jquery.themepunch.revolution.min.js"></script>
<!-- Slider revolution 5.0 Extensions  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="js/rev.slider.js"></script>
<script>
function process(e)
{
    if (!e) e = window.event;
    e.preventDefault();
    var harga_rumah  = parseInt(document.getElementById('harga_rumah').value);
    var uang_muka    = parseInt(document.getElementById('uang_muka').value);
    var margin       = parseFloat(document.getElementById('margin').value).toFixed(2);
    var tenor        = parseInt(document.getElementById('tenor').value);
    var penghasilan  = parseInt(document.getElementById('penghasilan').value);
    var cicilan_lain = parseInt(document.getElementById('cicilan_lain').value);
    const LIMIT = 40;
    // error if...
    // uang_muka >= harga_rumah
    if (uang_muka >= harga_rumah)
    {
        alert('Uang Muka tidak boleh lebih dari Harga Rumah');
        return ;
    }
    // uang_muka < 0.2 * harga_rumah
    if (uang_muka < 0.2 * harga_rumah)
    {
        alert('Uang Muka minimal 20 persen dari Harga Rumah');
        return ;
    }
    // penghasilan >= harga rumah - uang_muka
    if (penghasilan >= harga_rumah)
    {
        alert('Penghasilan per bulan lebih dari Harga Rumah');
        return ;
    }
    // cicilan_lain >= penghasilan
    if (cicilan_lain >= penghasilan)
    {
        alert('Cicilan Lain lebih dari Penghasilan per Bulan');
        return ;
    }
    var pinjaman = harga_rumah - uang_muka;
    var total_pinjaman = pinjaman + (margin / 100 * pinjaman * tenor);
    var cicilan_bulanan = parseInt(total_pinjaman / tenor / 12);
    var persentase_cicilan = parseFloat((cicilan_bulanan + cicilan_lain) / penghasilan * 100).toFixed(2);
    document.getElementById('hasil_harga_rumah').innerHTML = addCommas(harga_rumah);
    document.getElementById('hasil_uang_muka').innerHTML = addCommas(uang_muka);
    document.getElementById('hasil_margin').innerHTML = margin;
    document.querySelectorAll('.hasil_pinjaman')[0].innerHTML = document.querySelectorAll('.hasil_pinjaman')[1].innerHTML = addCommas(pinjaman);
    document.querySelectorAll('.hasil_tenor')[0].innerHTML = document.querySelectorAll('.hasil_tenor')[1].innerHTML = tenor;
    document.querySelectorAll('.hasil_pinjaman')[0].innerHTML = 
        document.querySelectorAll('.hasil_pinjaman')[1].innerHTML = 
        document.getElementById('pinjaman').innerHTML = addCommas(pinjaman);
    document.getElementById('hasil_total_pinjaman').innerHTML = 
        document.getElementById('total_pinjaman').innerHTML = addCommas(total_pinjaman);
    document.getElementById('cicilan_bulanan').innerHTML = addCommas(cicilan_bulanan);
    document.getElementById('persentase_cicilan').innerHTML = persentase_cicilan;
    if (persentase_cicilan > LIMIT)
        document.getElementById('persentase_cicilan').style.color = 'red';
    else
        document.getElementById('persentase_cicilan').style.color = 'green';
}
function addCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

    
jQuery(document).ready(function() {
	'use strict';
    dz_rev_slider_3();
   // window.onscroll = function() {scrollFunction()};

    jQuery(document).scroll(function(){
		console.log(jQuery(document).scrollTop());
    if (jQuery(document).scrollTop() > 300) {
    //     //  document.getElementById("logo-hvm").style.width = "70%";
        jQuery('#logo-hvm').animate({width:'70%'});
         
     } else {
    //     //document.getElementById("logo-hvm").style.width = "100%";
         jQuery('#logo-hvm').animate({width:'100%'});
     }

    });

  
  //  }
});	/*ready*/
</script>
</body>
</html>
