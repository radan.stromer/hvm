<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cendana Icon - Property</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
    <link rel="stylesheet" href="./vendor/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="./vendor/magnific-popup/magnific-popup.min.css">
    <link rel="stylesheet" href="./vendor/lightgallery/css/lightgallery.min.css">
    <link rel="stylesheet" href="./vendor/animate/animate.css">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="./css/style.css?v=35">
	<!-- REVOLUTION SLIDER CSS -->
	<link rel="stylesheet" type="text/css" href="./vendor/revolution/revolution/css/revolution.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

</head>

<body> 
	<div id="loading-area"></div>
	<div class="onepage"></div>
    <div class="page-wraper">
	
		<!-- header -->
		<header class="site-header header-transparent">
			<!-- <div class="top-bar">
				<div class="container-fluid">
					<div class="row d-flex justify-content-between align-items-center">
						<div class="dlab-topbar-left">
							<ul>
								<li><i class="la la-phone-volume"></i> 0811 1007 078</li>
								<li><i class="las la-map-marker"></i> 1073 W Stephen Ave, Clawson</li>
							</ul>
						</div>
						<div class="dlab-topbar-right">
							<ul>
								<li><i class="la la-clock"></i>  Mon - Sat 8.00 - 18.00</li>
								<li><i class="las la-envelope-open"></i> info@example.com</li>
							</ul>				
						</div>
					</div>
				</div>
			</div> -->
			<!-- main header -->
			<div class="sticky-header main-bar-wraper navbar-expand-lg">
				<div class="main-bar clearfix ">
					<div class="container-fluid clearfix">
					<!-- website logo -->
					<div class="logo-header mostion logo-dark">
						<a href="index.html"><img src="images/logo.png" alt=""></a>
					</div>
					<!-- nav toggle button -->
					<button class="navbar-toggler collapsed navicon justify-content-end " type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
						<span></span>
						<span></span>
						<span></span> 
					</button>
					<!-- extra nav -->
					<div class="extra-nav" style="position:fixed; right:5%;">
						<div class="extra-cell">
							<a href="#" class="btn btn-primary btnhover13">Daftar Sekarang</a>
						</div>
					</div>
					<div class="header-nav navbar-collapse collapse justify-content-end" id="navbarNavDropdown">
						<div class="logo-header d-md-block d-lg-none">
							<a href="index.html"><img src="images/logo.png" alt=""></a>
						</div>
						<div class="dlab-social-icon">
							<ul>
								<li><a class="site-button circle fa fa-facebook" href="javascript:void(0);"></a></li>
								<li><a class="site-button  circle fa fa-twitter" href="javascript:void(0);"></a></li>
								<li><a class="site-button circle fa fa-linkedin" href="javascript:void(0);"></a></li>
								<li><a class="site-button circle fa fa-instagram" href="javascript:void(0);"></a></li>
							</ul>
						</div>		
					</div>
				</div>
				</div>
			</div>
			<!-- main header END -->
		</header>
		<!-- header END -->
        <div class="sidenav-list">
			<ul class="navbar">
				<li><a class="scroll nav-link" href="#home"><i class="las la-home"></i> <span>Home</span></a></li>
				<li><a class="scroll nav-link" href="#specifications"><i class="las la-file-alt"></i> <span>Icon Cottage</span></a></li>
				<li><a class="scroll nav-link" href="#aboutUs"><i class="las la-user"></i> <span>ABOUT US</span></a></li>
				<li><a class="scroll nav-link" href="#masterPlan"><i class="las la-chart-bar"></i> <span>MASTER PLAN</span></a></li>
				
				<li><a class="scroll nav-link" href="#footer"><i class="las la-phone-volume"></i> <span>Contact Us</span></a></li>
			</ul>
			<!-- <div class="wlcm-form" style="">
						<div class="enter-form">
							<form action="index.html">
								<h2 class="wlcm-form-title">Dapatkan Info Ekslusif</h2>
								<div class="input-group form-group">
									<div class="input-group-prepend">
										<span class="las la-user"></span> 
									</div> 
									<input type="text" class="form-control" placeholder="Your Name" aria-label="Your Name">
								</div>
								<div class="input-group form-group">
									<div class="input-group-prepend">
										<span class="las las la-tty"></span>
									</div>
									<input type="text" class="form-control" placeholder="Phone No." aria-label="Phone No.">
								</div>
							
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
						</div>
						<div class="enter-button">
							
							<a href="#" class="btn btn-primary">Enter
								<div class="enter-animate">
								   <div class="arrow"></div><div class="arrow"></div><div class="arrow"></div>
								</div>
							</a>
						</div>
					</div> -->
		</div>
		<div class="page-content bg-white">
			<!-- Main Slider -->
			<div class="rev-slider" id="home">
				<div id="rev_slider_1164_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="exploration-header" data-source="gallery" style="background-color:transparent;padding:0px;">
					<!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
					<div id="rev_slider_1164_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
						<ul>	
							<!-- SLIDE  -->
							<li data-index="rs-3204" data-transition="slideoververtical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="images/main-slider/slider1.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="2000" data-fsslotamount="7" data-saveperformance="off"  data-title="" data-param1="What our team has found in the wild" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
								<!-- MAIN IMAGE -->
								<img src="images/main-slider/slider1.jpg"  alt=""  data-lazyload="images/main-slider/slider1.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="6" class="rev-slidebg" data-no-retina>
								<!-- LAYER NR. 1 -->
								<!-- LAYER NR. 2 -->
								<div class="tp-caption" 
									id="slide-3204-layer-1" 
									data-x="['center','center','center','center']" 
									data-hoffset="['-230','-230','0','0']" 
									data-y="['top','top','top','top']" 
									data-voffset="['250','250','350','200']" 
									data-width="['700','600','600','300']"
									data-fontsize="['55','55','30','28']"
									data-lineheight="['75','65','36','35']"
									data-height="none"
									data-whitespace="normal"
									data-type="text" 
									data-basealign="slide" 
									data-responsive_offset="on" 
									data-responsive="on"
									data-textAlign="['left','left','left','left']"
									data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":650,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"
									style="z-index: 5; white-space: normal; color:#000; font-family: 'Roboto Condensed', sans-serif; border-width:0px; font-weight:600; text-transform:uppercase;">
									   Helping you and your<br/>house become better<br/>acquainted 
								</div>
								<div class="tp-caption" 
									id="slide-3204-layer-2" 
									data-x="['center','center','center','center']" 
									data-hoffset="['-290','-290','-30','-50']" 
									data-y="['top','top','top','top']" 
									data-voffset="['200','200','315','170']" 
									data-width="['700','600','600','260']"
									data-fontsize="['55','45','30','24']"
									data-lineheight="['75','45','40','30']"
									data-height="none"
									data-whitespace="normal"
									data-type="text" 
									data-basealign="slide" 
									data-responsive_offset="on" 
									data-responsive="on"
									data-textAlign="['left','left','left','left']"
									data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":650,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"
									style="z-index: 4; white-space: normal; color:#000; font-family: 'Roboto Condensed', sans-serif; border-width:0px; font-weight:600; text-transform:uppercase;">
									<div class="rs-looped rs-wave"  data-speed="5" data-angle="0" data-radius="3px" data-origin="50% 50%">
									  <img src="images/main-slider/slider2.png" alt="" data-ww="['420px','420px','280px','240px']" data-hh="['415px','415px','280px','240px']" width="407" height="200" data-no-retina> 
									</div>
								</div>
								
								<!-- LAYER NR. 4 -->
								<div class="tp-caption tp-resizeme rs-parallaxlevel-1" 
									id="slide-100-layer-3" 
									data-x="['center','center','center','right']" data-hoffset="['0','0','0','0']" 
									data-y="['bottom','bottom','bottom','bottom']" data-voffset="['-30','-20','-10','-30']" 
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-type="image" 
									data-responsive_offset="on"
									data-responsive="on"									
									data-frames='[{"delay":250,"speed":5000,"frame":"0","from":"y:50px;rZ:5deg;opacity:0;fb:50px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
									data-textAlign="['inherit','inherit','inherit','inherit']"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"
									style="z-index: 3;">
									<div class="rs-looped rs-wave"  data-speed="5" data-angle="0" data-radius="3px" data-origin="50% 50%">
										<img src="images/main-slider/slider10.png" alt="" data-ww="['2000px','1300px','1100px','700px']" data-hh="['1000px','702px','594px','378px']" width="407" height="200" data-no-retina> 
									</div>
								</div>
								
								<!-- LAYER NR. 5 -->
								
								<div class="tp-caption rs-button" 
									id="slide-3204-layer-5" 
									data-x="['center','center','center','center']" 
									data-hoffset="['-225','-220','0','-20']" 
									data-y="['bottom','bottom','bottom','bottom']" 
									data-voffset="['100','150','200','200']" 
									data-width="['700','600','600','260']"
									data-fontsize="['55','45','30','24']"
									data-lineheight="['75','45','36','30']"
									data-height="none"
									data-whitespace="normal"
									data-type="text" 
									data-basealign="slide" 
									data-responsive_offset="on" 
									data-responsive="on"
									data-textAlign="['left','left','left','left']"
									data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":650,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"
									style="z-index: 4; white-space: normal; color:#000; font-family: 'Roboto Condensed', sans-serif; border-width:0px; font-weight:600; text-transform:uppercase;">
									<a href="media/video-1.mp4" class="popup-youtube slide-play-button"><i class="fa fa-play"></i></a>	
								</div>
							</li>
						
						
								<!-- SLIDE  -->
							<li data-index="rs-200" data-transition="slideoververtical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="images/main-slider/slider1.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="2000" data-fsslotamount="7" data-saveperformance="off"  data-title="" data-param1="What our team has found in the wild" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
								<!-- MAIN IMAGE -->
								<img src="images/main-slider/slider1.jpg"  alt=""  data-lazyload="images/main-slider/slider1.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="6" class="rev-slidebg" data-no-retina>
								<!-- LAYER NR. 1 -->
								<!-- LAYER NR. 2 -->
								<div class="tp-caption" 
									id="slide-200-layer-1" 
									data-x="['center','center','center','center']" 
									data-hoffset="['-230','-230','0','0']" 
									data-y="['top','top','top','top']" 
									data-voffset="['250','250','350','200']" 
									data-width="['700','600','600','300']"
									data-fontsize="['55','55','30','28']"
									data-lineheight="['75','65','36','35']"
									data-height="none"
									data-whitespace="normal"
									data-type="text" 
									data-basealign="slide" 
									data-responsive_offset="on" 
									data-responsive="on"
									data-textAlign="['left','left','left','left']"
									data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":650,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"
									style="z-index: 5; white-space: normal; color:#000; font-family: 'Roboto Condensed', sans-serif; border-width:0px; font-weight:600; text-transform:uppercase;">
									   Helping you and your<br/>house become better<br/>acquainted 
								</div>
								<div class="tp-caption" 
									id="slide-200-layer-2" 
									data-x="['center','center','center','center']" 
									data-hoffset="['-290','-290','-30','-50']" 
									data-y="['top','top','top','top']" 
									data-voffset="['200','200','315','170']" 
									data-width="['700','600','600','260']"
									data-fontsize="['55','45','30','24']"
									data-lineheight="['75','45','40','30']"
									data-height="none"
									data-whitespace="normal"
									data-type="text" 
									data-basealign="slide" 
									data-responsive_offset="on" 
									data-responsive="on"
									data-textAlign="['left','left','left','left']"
									data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":650,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"
									style="z-index: 4; white-space: normal; color:#000; font-family: 'Roboto Condensed', sans-serif; border-width:0px; font-weight:600; text-transform:uppercase;">
									<div class="rs-looped rs-wave"  data-speed="5" data-angle="0" data-radius="3px" data-origin="50% 50%">
									  <img src="images/main-slider/slider2.png" alt="" data-ww="['420px','420px','280px','240px']" data-hh="['415px','415px','280px','240px']" width="407" height="200" data-no-retina> 
									</div>
								</div>
								
								<!-- LAYER NR. 4 -->
								<div class="tp-caption tp-resizeme rs-parallaxlevel-1" 
									id="slide-200-layer-3" 
									data-x="['center','center','center','right']" data-hoffset="['0','0','0','0']" 
									data-y="['bottom','bottom','bottom','bottom']" data-voffset="['-30','-20','-10','-30']" 
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-type="image" 
									data-responsive_offset="on"
									data-responsive="on"									
									data-frames='[{"delay":250,"speed":5000,"frame":"0","from":"y:50px;rZ:5deg;opacity:0;fb:50px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
									data-textAlign="['inherit','inherit','inherit','inherit']"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"
									style="z-index: 3;">
									<div class="rs-looped rs-wave"  data-speed="5" data-angle="0" data-radius="3px" data-origin="50% 50%">
										<img src="images/main-slider/slider12.png" alt="" data-ww="['2000px','1300px','1100px','700px']" data-hh="['1000px','702px','594px','378px']" width="407" height="200" data-no-retina> 
									</div>
								</div>
								
								<!-- LAYER NR. 5 -->
								
								<div class="tp-caption rs-button" 
									id="slide-200-layer-5" 
									data-x="['center','center','center','center']" 
									data-hoffset="['-225','-220','0','-20']" 
									data-y="['bottom','bottom','bottom','bottom']" 
									data-voffset="['100','150','200','200']" 
									data-width="['700','600','600','260']"
									data-fontsize="['55','45','30','24']"
									data-lineheight="['75','45','36','30']"
									data-height="none"
									data-whitespace="normal"
									data-type="text" 
									data-basealign="slide" 
									data-responsive_offset="on" 
									data-responsive="on"
									data-textAlign="['left','left','left','left']"
									data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":650,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"
									style="z-index: 4; white-space: normal; color:#000; font-family: 'Roboto Condensed', sans-serif; border-width:0px; font-weight:600; text-transform:uppercase;">
									<a href="media/video-1.mp4" class="popup-youtube slide-play-button"><i class="fa fa-play"></i></a>	
								</div>
							</li>
						
						</ul>
						<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	
					</div>
				</div><!-- END REVOLUTION SLIDER -->
			</div>  
			<!-- Main Slider -->
			<section class="amenities-area">
				<div class="amenities-carousel owl-carousel owl-btn-center-lr">
					<div class="items wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
						<div class="amenit-box">
							<div class="media">
								<img src="images/PPT---E-FLYER.jpg" alt=""/>
							</div>
							<div class="info">
								<h5 class="title">
									<i class="ti-home"></i>
									<span>Club House</span>
								</h5>
							</div>
						</div>
					</div>
					<div class="items wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.4s">
						<div class="amenit-box">
							<div class="media">
								<img src="images/CUSTOMER-REWARD-E-FLYER.jpg" alt=""/>
							</div>
							<div class="info">
								<h5 class="title">
									<i class="ti-home"></i>
									<span>Club House</span>
								</h5>
							</div>
						</div>
					</div>
					<div class="items wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">
						<div class="amenit-box">
							<div class="media">
								<img src="images/SALES-PROGRAM-E-FLYER.jpg" alt=""/>
							</div>
							<div class="info">
								<h5 class="title">
									<i class="ti-home"></i>
									<span>Club House</span>
								</h5>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="content-inner-2" data-content="SPECIFICATIONS" id="specifications">				
				<div class="container">
					<div class="section-head head-col">
						<h2 class="title wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s">Icon Cottage</h2>
						<p class=" wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</p>
					</div>
				</div>
				<div class="row faqs-box spno">
					<div class="col-md-6 col-lg-6 col-xl-6">
						<div class="faq-media wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.6s">
							<img src="images/specifications/tipe3.jpg" id="Capmap1" class="active" alt=""/>
							<img src="images/specifications/tipe3-1.jpg" id="Capmap2" alt=""/>
							<img src="images/specifications/tipe3-2.jpg" id="Capmap3" alt=""/>
						</div>
					</div>
					<div class="col-md-6 col-lg-6 col-xl-6 col-xxxl-2 faq-list">
						<div class="accordion" id="faqSpecifications">
							
						<div class="wlcm-form">
						<div class="enter-form" style="width:100%;">
							<form action="index2.php">
								<h2 class="wlcm-form-title">Dapatkan infromasi terkini</h2>
								<div class="input-group form-group">
									<div class="input-group-prepend">
										<span class="las la-user"></span>
									</div>
									<input type="text" class="form-control" placeholder="Nama" aria-label="Your Name" required/>
								</div>
								<div class="input-group form-group">
									<div class="input-group-prepend">
										<span class="las la-envelope"></span>
									</div>
									<input type="email" class="form-control" placeholder="Email" aria-label="Your Name" required/>
								</div>
								<div class="input-group form-group">
									<div class="input-group-prepend">
										<span class="las las la-tty"></span>
									</div>
									<input type="text" class="form-control" placeholder="Phone No." aria-label="Phone No." required/>
								</div>
								<div class="input-group form-group">
									<div class="input-group-prepend">
										<span class="la money-bill"></span>
									</div>
									<select class="form-control">
										<option>Harga Property yang dicari</option>
										<option value="1">< 500 Juta</option>
										<option value="2">500 Juta -  1 Milyar</option>
										<option value="3">1 Milyar - 1,5 Milyar</option>
									</select>
								</div>
								<div class="input-group form-group">
									<div class="input-group-prepend">
										<span class="la la-home"></span>
									</div>
									<select class="form-control">
										<option>Tipe Property yang disuka</option>
										<option value="1">Cendana Villa</option>
										<option value="2">Cendana Residence</option>
										<option value="3">Cendana COTTAGE</option>
									</select>
								</div>
							
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
						</div>
						<!-- <div class="enter-button">
							
							<a href="index2.php" class="btn btn-primary">Enter
								<div class="enter-animate">
								   <div class="arrow"></div><div class="arrow"></div><div class="arrow"></div>
								</div>
							</a>
						</div> -->
					</div>
							
						</div>
					</div>
				</div>
			</section>
			<section class="content-inner about-box" data-content="ABOUT US" id="aboutUs">	
				<div class="about-bg"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-7 col-lg-6">
							<div class="section-head">
								<h2 class="title wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">Projects Overview</h2>
								<div class="dlab-separator bg-primary  wow fadeInUp" data-wow-duration="4s" data-wow-delay="0.2s"></div>
								<h4 class="mb-4 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">SEE WHY OUR RESIDENTS CALL OUR COMMUNITY HOME.</h4>
								<p class=" wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.8s">Surround yourself with fresh energy, high-tech amenities, and elevated style. Indulge in extraordinary amenities, relax in appealing social spaces, and cultivate your ideal life. We're the fresh look in this historic district - a vibrant new community for movers and shakers.</p>
								<p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">A bold new life awaits you at KingArchitect, a brand new community of apartment homes situated at the cutting edge of modern design. Residents of KingArchitect enjoy luxury living with a sparkling swimming pool, fitness center and indoor game, parking garage and temple. Discover a KingArchitect from our convenient location. Nothing quite complements a comfortable, stylish home like an array of luxury amenities.</p>
							</div>
							<a href="about-us-1.html" class="btn btn-primary wow fadeInUp" data-wow-duration="2s" data-wow-delay="1.2s">About Us</a>
						</div>
						<div class="col-md-5 col-lg-6"></div>
					</div>
				</div>
			</section>
			<section class="content-inner" data-content="MASTER PLAN" id="masterPlan">				
				<div class="setResizeMargin master-plan">
					<div class="row">
						<div class="col-md-12 col-lg-6">
							<div class="section-head">
								<h2 class="title wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">SITE PLAN & MASTER PLAN</h2>
								<div class="dlab-separator bg-primary"></div>
								<p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.5s"> Rumah 2 lantai di Chapter terbaru dari Lippo Village. Juga tersedia tipe hoek yang luas tanahnya lebih lebar dengan luas bangunan yang sama. Dengan memiliki tipe Hoek (Rumah Sudut) kamu bisa menikmati taman di samping rumah kamu untuk koleksi tanamanan kesayangan kamu sambil bersantai bersama keluarga, seperti Tea Time di sore hari.</p>
							</div>	
							<div class="row">
								<div class="col-md-4 col-sm-4">
									<ul class="nav plan-tabs wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.5s" id="myTab" role="tablist">
										<li class="nav-item" role="presentation">
											<a class="nav-link active" id="main-tab" data-toggle="tab" href=".iconvilla" role="tab" aria-controls="main" aria-selected="true">Icon Villa</a>
										</li>
										<li class="nav-item" role="presentation">
											<a class="nav-link" id="profile-tab" data-toggle="tab" href=".iconresidence" role="tab" aria-controls="profile" 	aria-selected="false">
												Icon Residence
											</a>
										</li>
										<li class="nav-item" role="presentation">
											<a class="nav-link" id="contact-tab" data-toggle="tab" href=".iconcottage" role="tab" aria-controls="contact" aria-selected="false">Icon Cottage</a>
										</li>
									</ul>
								</div>
								<div class="col-md-8 col-sm-8 tab-content">
									<div class="tab-pane plan-contant show active iconvilla" role="tabpanel">
										<div class="section-head">
											<h2 class="title">5 x 12 M</h2>
											<h4 class="mb-lg-2 mb-xl-4">Luas Tanah 60 M<sup>2</sup>, Luas Bangunan 60 M<sup>2</sup></h4>
											<p>USP: Reduced energy usage and costs, Reduced water usage through low flow fixtures, Improved indoor air quality through the usage of low - VOC paints, adhesives and eco-friendly chemicals</p>
										</div>
										<ul class="flat-plan">
											<li>
												<img src="images/icons/icon-1.png" alt=""/>
												<h3>60 M<sup>2</sup></h3>
												<span>Luas Tanah</span>
											</li>
											<li>
												<img src="images/icons/icon-2.png" alt=""/>
												<h3>2</h3>
												<span>Bathrooms</span>
											</li>
											<li>
												<img src="images/icons/icon-3.png" alt=""/>
												<h3>3 + 1</h3>
												<span>Bedrooms</span>
											</li>
											<li>
												<img src="images/icons/icon-4.png" alt=""/>
												<h3>01</h3>
												<span>Balcony</span>
											</li>
										</ul>
										<a href="#" class="call-planner"><i class="la la-phone-volume"></i>0811 1007 078</a>
									</div>

									<div class="tab-pane plan-contant iconresidence"   role="tabpanel">
										<div class="section-head">
											<h2 class="title">5,5 x 15 M</h2>
											<h4 class="mb-lg-2 mb-xl-4">Luas Tanah 82,5 M<sup>2</sup>, Luas Bangunan 82 M<sup>2</sup></h4>
											<p>USP: Reduced energy usage and costs, Reduced water usage through low flow fixtures, Improved indoor air quality through the usage of low - VOC paints, adhesives and eco-friendly chemicals</p>
										</div>
										<ul class="flat-plan">
											<li>
												<img src="images/icons/icon-1.png" alt=""/>
												<h3>82 M<sup>2</sup></h3>
												<span>Luas Tanah</span>
											</li>
											<li>
												<img src="images/icons/icon-2.png" alt=""/>
												<h3>3.5</h3>
												<span>Bathrooms</span>
											</li>
											<li>
												<img src="images/icons/icon-3.png" alt=""/>
												<h3>3</h3>
												<span>Bedrooms</span>
											</li>
											<li>
												<img src="images/icons/icon-4.png" alt=""/>
												<h3>01</h3>
												<span>Balcony</span>
											</li>
										</ul>
										<a href="#" class="call-planner"><i class="la la-phone-volume"></i>0811 1007 078</a>
									</div>

									<div class="tab-pane plan-contant iconcottage" role="tabpanel">
										<div class="section-head">
											<h2 class="title">6,5 x 15 M</h2>
											<h4 class="mb-lg-2 mb-xl-4">Luas Tanah 97,5 M<sup>2</sup>, Luas Bangunan 88 M<sup>2</sup></h4>
											<p>USP: Reduced energy usage and costs, Reduced water usage through low flow fixtures, Improved indoor air quality through the usage of low - VOC paints, adhesives and eco-friendly chemicals</p>
										</div>
										<ul class="flat-plan">
											<li>
												<img src="images/icons/icon-1.png" alt=""/>
												<h3>97,5 M<sup>2</sup></h3>
												<span>Luas Tanah</span>
											</li>
											<li>
												<img src="images/icons/icon-2.png" alt=""/>
												<h3>3.5</h3>
												<span>Bathrooms</span>
											</li>
											<li>
												<img src="images/icons/icon-3.png" alt=""/>
												<h3>4 + 1</h3>
												<span>Bedrooms</span>
											</li>
											<li>
												<img src="images/icons/icon-4.png" alt=""/>
												<h3>01</h3>
												<span>Balcony</span>
											</li>
										</ul>
										<a href="#" class="call-planner"><i class="la la-phone-volume"></i>0811 1007 078</a>
									</div>
								</div> 
							</div>
						</div>
						<div class="col-md-12 col-lg-6">
							<div class="tab-content plan-preview wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.5s" id="myTabContent">
								<div class="tab-pane fade show active iconvilla" role="tabpanel" aria-labelledby="main-tab">
									<img src="images/plan/tipe-1.jpg" alt=""/>
								</div>
								<div class="tab-pane fade iconresidence" role="tabpanel" aria-labelledby="profile-tab">
									<img src="images/plan/tipe-2.jpg" alt=""/>
								</div>
								<div class="tab-pane fade iconcottage" role="tabpanel" aria-labelledby="contact-tab">
									<img src="images/plan/tipe-3.jpg" alt=""/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="content-inner-1" data-content="ABOUT US">				
				<div class="container">
					<div class="section-head text-center">
						<h2 class="title">25+ Years Of Experience</h2>
						<p>Surround yourself with fresh energy, high-tech amenities, and elevated style. Indulge in extraordinary amenities, relax in appealing social spaces, and cultivate your ideal life. We're the fresh look in this historic district - a vibrant new community for movers and shakers.</p>
					</div>
					<div class="row">
						<div class="col-md-4 col-lg-2 col-6 mb-4">
							<div class="aminite-icon-bx">
								<i class="flaticon-drama"></i>
								<h4 class="title">Activity Room</h4>
							</div>
						</div>
						<div class="col-md-4 col-lg-2 col-6 mb-4 mt-lg-3 mt-0">
							<div class="aminite-icon-bx">
								<i class="flaticon-cinema"></i>
								<h4 class="title">mini Theater</h4>
							</div>
						</div>
						<div class="col-md-4 col-lg-2 col-6 mb-4 mt-lg-5 mt-0">
							<div class="aminite-icon-bx">
								<i class="flaticon-stationary-bike"></i>
								<h4 class="title">Fitnesh Gym </h4>
							</div>
						</div>
						<div class="col-md-4 col-lg-2 col-6 mb-4 mt-lg-5 mt-0">
							<div class="aminite-icon-bx">
								<i class="flaticon-round-table"></i>
								<h4 class="title">Multipurpose Hall</h4>
							</div>
						</div>
						<div class="col-md-4 col-lg-2 col-6 mb-4 mt-lg-3 mt-0">
							<div class="aminite-icon-bx">
								<i class="flaticon-gamepad"></i>
								<h4 class="title">Games Room</h4>
							</div>
						</div>
						<div class="col-md-4 col-lg-2 col-6 mb-4">
							<div class="aminite-icon-bx">
								<i class="flaticon-reading-book"></i>
								<h4 class="title">library</h4>
							</div>
						</div>
					</div>
				</div>
			</section>
		
			<div class="map-view">
				<div class="row spno">
					<div class="col-md-6 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="1s"><img src="images/map.jpg" alt=""/></div>
					<div class="col-md-6 wow fadeInRight" data-wow-duration="2s" data-wow-delay="1s"><img src="images/site-map-2u.jpg" alt=""/></div>
				</div>
			</div>
		</div>
		
       <!-- Footer -->
    <footer class="site-footer" id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
					<div class="col-md-4 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.3s">
                        <div class="widget widget_about">
							<div class="footer-logo">
								<a href="index.html"><img src="images/logo.png" alt=""/></a> 
							</div>
							<p>Surround yourself with fresh energy, high-tech amenities, and elevated style. Indulge in extraordinary amenities, relax in appealing social spaces.</p>
							<div class="dlab-social-icon"> 
								<ul>
									<li><a class="fa fa-facebook" href="javascript:void(0);"></a></li>
									<li><a class="fa fa-twitter" href="javascript:void(0);"></a></li>
									<li><a class="fa fa-linkedin" href="javascript:void(0);"></a></li>
									<li><a class="fa fa-instagram" href="javascript:void(0);"></a></li>
								</ul>
							</div>
						</div>
                    </div>
					<div class="col-md-4 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.6s">
						<div class="widget">
							<h5 class="footer-title">Contact Us</h5>
							<ul class="contact-info-bx">
								<li><i class="las la-map-marker"></i><strong>Address</strong> 1st Floor, 123 West Street, Melbourne Victoria 3000 Australia</li>
								<li><i class="las la-phone-volume"></i><strong>Call :-</strong>    0811 1007 078</li>
								<li><i class="las la-phone-square-alt"></i><strong>Board line :-</strong> 0123-4567890</li>
							</ul>
						</div>
                    </div>
					<div class="col-md-4 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.9s">
						<div class="widget widget-logo">
						   <h5 class="footer-title">Our Business Channels</h5>
						   <ul>
								<li><a href="javascript:void(0);"><img src="images/logo/logo1.png" alt=""/></a></li>
								<li><a href="javascript:void(0);"><img src="images/logo/logo2.png" alt=""/></a></li>
								<li><a href="javascript:void(0);"><img src="images/logo/logo3.png" alt=""/></a></li>
								<li><a href="javascript:void(0);"><img src="images/logo/logo4.png" alt=""/></a></li>
						   </ul>
						</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer bottom part -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 text-md-left text-center"> <span>© <?=$date('Y');?> Cendana Icon. All Right Reserved</span> </div>
                    <div class="col-md-6 col-sm-12 text-md-right text-center"> 
						<div class="widget-link "> 
							<ul>
								<li><a href="about-us-1.html"> About</a></li>
								<li><a href="contact-us-1.html"> Contact Us</a></li>
								<li><a href="privacy-policy.html"> Privacy Policy</a></li>
							</ul>
						</div>
					</div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer END-->

    </div>



<!-- JAVASCRIPT FILES ========================================= -->
<script src="js/jquery.min.js"></script><!-- JQUERY.MIN JS -->
<script src="vendor/wow/wow.js"></script><!-- WOW JS -->
<script src="vendor/bootstrap/js/popper.min.js"></script><!-- POPPER.MIN JS -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->
<script src="vendor/owl-carousel/owl.carousel.js"></script><!-- OWL-CAROUSEL JS -->
<script src="vendor/magnific-popup/magnific-popup.js"></script><!-- MAGNIFIC-POPUP JS -->
<script src="vendor/counter/waypoints-min.js"></script><!-- WAYPOINTS JS -->
<script src="vendor/counter/counterup.min.js"></script><!-- COUNTERUP JS -->
<script src="vendor/imagesloaded/imagesloaded.js"></script><!-- IMAGESLOADED -->
<script src="vendor/masonry/masonry-3.1.4.js"></script><!-- MASONRY -->
<script src="vendor/masonry/masonry.filter.js"></script><!-- MASONRY -->
<script src="vendor/lightgallery/js/lightgallery-all.min.js"></script><!-- LIGHTGALLERY -->
<script src="js/dz.carousel.js"></script><!-- OWL-CAROUSEL -->
<script src="js/custom.js?v=35"></script><!-- CUSTOM JS -->

<!-- REVOLUTION JS FILES -->
<script src="vendor/revolution/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/revolution/revolution/js/jquery.themepunch.revolution.min.js"></script>
<!-- Slider revolution 5.0 Extensions  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="vendor/revolution/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="js/rev.slider.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<script>
jQuery(document).ready(function() {
	'use strict';
	dz_rev_slider_1();
});	/*ready*/
</script>
</body>
</html>