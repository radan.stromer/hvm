<?php 
	if($_POST){
		echo 'test'; exit;

	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cendana Icon</title>
	<!-- Favicon icon -->
	
	<link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="./vendor/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="./vendor/magnific-popup/magnific-popup.min.css">
    <link rel="stylesheet" href="./vendor/lightgallery/css/lightgallery.min.css">
    <link rel="stylesheet" href="./vendor/animate/animate.css">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="./css/style.css?v=<?=date('YmdHis');?>">
	<!-- REVOLUTION SLIDER CSS -->
	<link rel="stylesheet" type="text/css" href="./vendor/revolution/revolution/css/revolution.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
	<style>
		#img{
			width:80%; height:80vh; margin:0 auto; display:block;
			@media only screen and (max-width: 600px) {
				height:auto; 
			}
		}
		.flat-plan img{
			width:50px;
			@media only screen and (max-width: 600px) {
				width:40px !important; 
			}
		}
	</style>
	
</head>

<body>
	<div id="loading-area"></div>
	<div class="onepage"></div>
    <div class="page-wraper">
		<!-- header -->
		<header class="site-header header-transparent">
			<!-- main header -->
			<div class="sticky-header main-bar-wraper navbar-expand-lg">
				<div class="main-bar clearfix">
					<div class="container-fluid clearfix">
					<!-- website logo -->
					<!-- website logo -->
					<div class="logo-header mostion logo-dark">
							<a href="index.html"><img src="images/logo.png" alt=""></a>
					</div>
					
					<!-- nav toggle button -->
					
					<!-- extra nav -->
					<div class="extra-nav">
						<div class="extra-cell">
							<a href="#specifications" class="btn btn-primary d-none d-sm-block btnhover13">HUBUNGI KAMI</a>
							<a data-bs-toggle="modal" data-bs-target="#staticBackdrop" class="btn btn-primary d-block d-sm-none btnhover13">HUBUNGI KAMI</a>
						</div>
					</div>
					<div class="header-nav navbar-collapse collapse justify-content-end" id="navbarNavDropdown">
						<div class="logo-header d-md-block d-lg-none">
							<a href="index.html"><img src="images/logo.png" alt=""></a>
						</div>
					</div>
				</div>
				</div>
			</div>
			<!-- main header END -->
		</header>
			 <div class="container">
				<div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
					<div class="carousel-indicators">
						<button type="button" data-bs-target="#carouselExampleControls" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
						<button type="button" data-bs-target="#carouselExampleControls" data-bs-slide-to="1" aria-label="Slide 2"></button>
					</div>
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img style=" margin:0 auto;" class="d-block" src="images/main-slider/slider10.png" alt="...">
						</div>
						<div class="carousel-item">
							<img style="margin:0 auto;" class="d-block" src="images/main-slider/slider12.png" alt="...">
						</div>
					</div>
				</div>
			</div>      
		<!-- Slider END -->
		<div class="container">	
			<section class="content-inner-2 d-none d-sm-block" data-content="" id="specifications">				
				<div class="container">
					<div class="section-head head-col">
						<h2 class="title wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s">Penawaran Terbatas</h2>
						<p class=" wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">Segera isikan form penawaraan untuk mendapatkan update info terbaru dari kami.</p>
					</div>
				</div>
				<div class="row faqs-box spno ">
					<div class="col-md-6 col-lg-6 col-xl-6">
						<!-- Main Slider -->
						<section class="amenities-area">
							<div class="amenities-carousel owl-carousel owl-btn-center-lr">
								<div class="items wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
									<div class="amenit-box">
										<div class="media">
											<img src="images/gallery/depan-tipe-1.jpg" alt=""/>
										</div>
										<div class="info">
											<h5 class="title">
												<i class="ti-home"></i>
												<span>Icon Villa</span>
											</h5>
										</div>
									</div>
								</div>
								<div class="items wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.4s">
									<div class="amenit-box">
										<div class="media">
										<img src="images/gallery/depan-tipe-2.jpg" alt=""/>
										</div>
										<div class="info">
											<h5 class="title">
												<i class="ti-home"></i>
												<span>Icon Residence</span>
											</h5>
										</div>
									</div>
								</div>
								<div class="items wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">
									<div class="amenit-box">
										<div class="media">
											<img src="images/gallery/depan-tipe-3.jpg" alt=""/>
										</div>
										<div class="info">
											<h5 class="title">
												<i class="ti-home"></i>
												<span>Icon Cottage</span>
											</h5>
										</div>
									</div>
								</div>
								<div class="items wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">
									<div class="amenit-box">
										<div class="media">
											<img src="images/commercial.jpg" alt=""/>
										</div>
										<div class="info">
											<h5 class="title">
												<i class="ti-home"></i>
												<span>Icon Plaza</span>
											</h5>
										</div>
									</div>
								</div>
							</div>
						</section>
						<!-- Main Slider -->

					

					</div>
					<div class="col-md-6 col-lg-6 col-xl-6 faq-list" id="desktop-form">
						<div class="accordion" id="faqSpecifications">
							
						<div class="wlcm-form">
						<div class="enter-form" style="width:100%;">
							<form action="./contact.php" method="post">
								<h2 class="wlcm-form-title">Dapatkan infromasi terkini</h2>
								<div class="input-group form-group">
									<div class="input-group-prepend">
										<span class="las la-user"></span>
									</div>
									<input name="nama" type="text" class="form-control" placeholder="Nama" aria-label="Your Name" required/>
								</div>
								<div class="input-group form-group">
									<div class="input-group-prepend">
										<span class="las la-envelope"></span>
									</div>
									<input name="email" type="email" class="form-control" placeholder="Email" aria-label="Your Name" required/>
								</div>
								<div class="input-group form-group">
									<div class="input-group-prepend">
										<span class="las las la-tty"></span>
									</div>
									<input name="telp" type="text" class="form-control" placeholder="Phone No." aria-label="Phone No." required/>
								</div>
								<div class="input-group form-group">
									<div class="input-group-prepend">
										<span class="la la-money"></span>
									</div>
									<select name="harga" class="form-control">
										<option>Harga Property yang dicari</option>
										<option>< 500 Juta</option>
										<option>500 Juta -  1 Milyar</option>
										<option>1 Milyar - 1,5 Milyar</option>
									</select>
								</div>
								<div class="input-group form-group">
									<div class="input-group-prepend">
										<span class="la la-home"></span>
									</div>
									<select name="tipe" class="form-control">
										<option>Tipe Property yang disuka</option>
										<option>Cendana Villa</option>
										<option>Cendana Residence</option>
										<option>Cendana COTTAGE</option>
									</select>
								</div>
							
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
						</div>
						<!-- <div class="enter-button">
							
							<a href="index2.php" class="btn btn-primary">Enter
								<div class="enter-animate">
								   <div class="arrow"></div><div class="arrow"></div><div class="arrow"></div>
								</div>
							</a>
						</div> -->
					</div>
							
						</div>
					</div>
				</div>
			</section>
			<!-- <section class="content-inner about-box" data-content="ABOUT US" id="aboutUs">	
				<div class="about-bg"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-7 col-lg-6">
							<div class="section-head">
								<h2 class="title wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">Projects Overview</h2>
								<div class="dlab-separator bg-primary  wow fadeInUp" data-wow-duration="4s" data-wow-delay="0.2s"></div>
								<h4 class="mb-4 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">SEE WHY OUR RESIDENTS CALL OUR COMMUNITY HOME.</h4>
								<p style="text-shadow: 1px 1px 0px #000000;" class=" wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.8s">Perumahan Cendana Icon Lippo Karawaci mempunyai Keunggulan diantaranya adalah Sarana Fasilitas Yang Super Komplet, dimulai dari Sarana Teritori sampai Sarana Klaster yang akan penuhi dan lengkapi keperluan beberapa Penghuni Klaster Cendana Icon.</p>
								<p style="text-shadow: 1px 1px 0px #000000;" class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">Sarana Umum yang ada salah satunya Pusat Perbelanjaan seperti Supermall Karawaci, Maxx Box, Benton Junction, Hypermart, Villa Permata Culinary Center dan ada banyak lain nya.</p>
							</div>
							<a href="about-us-1.html" class="btn btn-primary wow fadeInUp" data-wow-duration="2s" data-wow-delay="1.2s">About Us</a>
						</div>
						<div class="col-md-5 col-lg-6"></div>
					</div>
				</div>
			</section> -->
			<section class="content-inner" data-content="TYPE" id="masterPlan">	
			<div id="galleryBottom" class="owl-carousel owl-theme owl-none owl-dots-none gallery-bottom container owl-btn-center-lr">
					<div class="item">
						<div class="gallery-box">
							<img src="images/gallery/depan-tipe-1.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="gallery-box">
							<img src="images/gallery/depan-tipe-2.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="gallery-box">
							<img src="images/gallery/depan-tipe-3.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="gallery-box">
							<img src="images/commercial.jpg" alt=""/>
						</div>
					</div>
					
				</div>		
			<div id="galleryTop" class="owl-carousel owl-theme owl-none owl-dots-none gallery-top">
					<div class="item">
						<div class="gallery-media">
							

							<div class="container">
							<h1 class="text-center">Icon Villa</h1>
							<div class="row">
								<div class="col-md-6 col-lg-6">
									<p class="d-none d-sm-block"> Rumah 2 lantai di Chapter terbaru dari Lippo Village. Juga tersedia tipe hoek yang luas tanahnya lebih lebar dengan luas bangunan yang sama. Dengan memiliki tipe Hoek (Rumah Sudut) kamu bisa menikmati taman di samping rumah kamu untuk koleksi tanamanan kesayangan kamu sambil bersantai bersama keluarga, seperti Tea Time di sore hari.</p>


									<div class="section-head">
											<h2>5 x 12 M</h2>
											<h4 class="mb-lg-2 mb-xl-4">Luas Tanah 60 M<sup>2</sup>, Luas Bangunan 60 M<sup>2</sup></h4>
											<p class="d-none d-sm-block">USP: Reduced energy usage and costs, Reduced water usage through low flow fixtures, Improved indoor air quality through the usage of low - VOC paints, adhesives and eco-friendly chemicals</p>
										</div>
										<ul class="flat-plan">
											<li>
												<img style="width:40px" src="images/icons/icon-1.png" alt=""/>
												<h3>60 M<sup>2</sup></h3>
												<span>Luas Tanah</span>
											</li>
											<li>
												<img style="width:40px" src="images/icons/icon-2.png" alt=""/>
												<h3>2</h3>
												<span>Bathrooms</span>
											</li>
											<li>
												<img style="width:40px" src="images/icons/icon-3.png" alt=""/>
												<h3>3 + 1</h3>
												<span>Bedrooms</span>
											</li>
											<li>
												<img style="width:40px" src="images/icons/icon-4.png" alt=""/>
												<h3>01</h3>
												<span>Balcony</span>
											</li>
											<li>
												<div style="position:absolute; left:0px;"><i style="color:#a6611c;font-size:38px;" class="las la-car-side"></i></div>
												<h3>01</h3>
												<span>Car Pot</span>
											</li>
										</ul>
								</div>
								<div class="col-md-6 col-lg-6">
									<img src="images/plan/tipe-1.jpg" alt=""/>
								</div>
							</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="gallery-media">
							<div class="container">
							<h1 class="text-center">Icon Residence</h1>
								<div class="row">
									<div class="col-md-6 col-lg-6">
										<p class="d-none d-sm-block"> Rumah 2 lantai di Chapter terbaru dari Lippo Village. Juga tersedia tipe hoek yang luas tanahnya lebih lebar dengan luas bangunan yang sama. Dengan memiliki tipe Hoek (Rumah Sudut) kamu bisa menikmati taman di samping rumah kamu untuk koleksi tanamanan kesayangan kamu sambil bersantai bersama keluarga, seperti Tea Time di sore hari.</p>


										<div class="section-head">
												<h2>5 x 12 M</h2>
												<h4 class="mb-lg-2 mb-xl-4">Luas Tanah 60 M<sup>2</sup>, Luas Bangunan 60 M<sup>2</sup></h4>
												<p class="d-none d-sm-block">USP: Reduced energy usage and costs, Reduced water usage through low flow fixtures, Improved indoor air quality through the usage of low - VOC paints, adhesives and eco-friendly chemicals</p>
											</div>
											<ul class="flat-plan">
												<li>
													<img style="width:40px" src="images/icons/icon-1.png" alt=""/>
													<h3>60 M<sup>2</sup></h3>
													<span>Luas Tanah</span>
												</li>
												<li>
													<img style="width:40px" src="images/icons/icon-2.png" alt=""/>
													<h3>2</h3>
													<span>Bathrooms</span>
												</li>
												<li>
													<img style="width:40px" src="images/icons/icon-3.png" alt=""/>
													<h3>3 + 1</h3>
													<span>Bedrooms</span>
												</li>
												<li>
													<img style="width:40px" src="images/icons/icon-4.png" alt=""/>
													<h3>01</h3>
													<span>Balcony</span>
												</li>
												<li>
													<div style="position:absolute; left:0px;"><i style="color:#a6611c;font-size:38px;" class="las la-car-side"></i></div>
													<h3>01</h3>
													<span>Car Pot</span>
												</li>
											</ul>
									</div>
									<div class="col-md-6 col-lg-6">
										<img src="images/plan/tipe-2.jpg" alt=""/>
									</div>
								</div>
							</div>
						
						</div>
					</div>
					<div class="item">
						<div class="gallery-media">
							<div class="container">
							<h1>Icon Cottage</h1>
								<div class="row">
									<div class="col-md-6 col-lg-6">
										<p class="d-none d-sm-block"> Rumah 2 lantai di Chapter terbaru dari Lippo Village. Juga tersedia tipe hoek yang luas tanahnya lebih lebar dengan luas bangunan yang sama. Dengan memiliki tipe Hoek (Rumah Sudut) kamu bisa menikmati taman di samping rumah kamu untuk koleksi tanamanan kesayangan kamu sambil bersantai bersama keluarga, seperti Tea Time di sore hari.</p>


										<div class="section-head">
												<h2>5 x 12 M</h2>
												<h4 class="mb-lg-2 mb-xl-4">Luas Tanah 60 M<sup>2</sup>, Luas Bangunan 60 M<sup>2</sup></h4>
												<p class="d-none d-sm-block">USP: Reduced energy usage and costs, Reduced water usage through low flow fixtures, Improved indoor air quality through the usage of low - VOC paints, adhesives and eco-friendly chemicals</p>
											</div>
											<ul class="flat-plan">
												<li>
													<img style="width:40px" src="images/icons/icon-1.png" alt=""/>
													<h3>60 M<sup>2</sup></h3>
													<span>Luas Tanah</span>
												</li>
												<li>
													<img style="width:40px" src="images/icons/icon-2.png" alt=""/>
													<h3>2</h3>
													<span>Bathrooms</span>
												</li>
												<li>
													<img style="width:40px" src="images/icons/icon-3.png" alt=""/>
													<h3>3 + 1</h3>
													<span>Bedrooms</span>
												</li>
												<li>
													<img style="width:40px" src="images/icons/icon-4.png" alt=""/>
													<h3>01</h3>
													<span>Balcony</span>
												</li>
												<li>
													<div style="position:absolute; left:0px;"><i style="color:#a6611c;font-size:38px;" class="las la-car-side"></i></div>
													<h3>01</h3>
													<span>Car Pot</span>
												</li>
												
											</ul>
									</div>
									<div class="col-md-6 col-lg-6">
										<img src="images/plan/tipe-3.jpg" alt=""/>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<div class="item">
						<div class="gallery-media">
							<div class="container">
								<div class="row">
								<h1>Icon Plaza</h1>
									<div class="col-md-6 col-lg-6">
										<p class="d-none d-sm-block"> Rumah 2 lantai di Chapter terbaru dari Lippo Village. Juga tersedia tipe hoek yang luas tanahnya lebih lebar dengan luas bangunan yang sama. Dengan memiliki tipe Hoek (Rumah Sudut) kamu bisa menikmati taman di samping rumah kamu untuk koleksi tanamanan kesayangan kamu sambil bersantai bersama keluarga, seperti Tea Time di sore hari.</p>


										<div class="section-head">
												<h2>5 x 12 M</h2>
												<h4 class="mb-lg-2 mb-xl-4">Luas Tanah 60 M<sup>2</sup>, Luas Bangunan 60 M<sup>2</sup></h4>
												<p class="d-none d-sm-block">USP: Reduced energy usage and costs, Reduced water usage through low flow fixtures, Improved indoor air quality through the usage of low - VOC paints, adhesives and eco-friendly chemicals</p>
											</div>
											<ul class="flat-plan">
												<li>
													<img style="width:40px" src="images/icons/icon-1.png" alt=""/>
													<h3>60 M<sup>2</sup></h3>
													<span>Luas Tanah</span>
												</li>
												<li>
													<img style="width:40px" src="images/icons/icon-2.png" alt=""/>
													<h3>2</h3>
													<span>Bathrooms</span>
												</li>
												<li>
													<img style="width:40px" src="images/icons/icon-3.png" alt=""/>
													<h3>3 + 1</h3>
													<span>Bedrooms</span>
												</li>
												<li>
													<img style="width:40px" src="images/icons/icon-4.png" alt=""/>
													<h3>01</h3>
													<span>Balcony</span>
												</li>
												<li>
													<div style="position:absolute; left:0px;"><i style="color:#a6611c;font-size:38px;" class="las la-car-side"></i></div>
													<h3>01</h3>
													<span>Car Pot</span>
												</li>
												<li>
													<img style="color:#a6611c;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgNTEyIDUxMiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMjsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPGc+DQoJPGc+DQoJCTxnPg0KCQkJPHBhdGggZD0iTTUwNCwzMDRIMzQ0di0xNmg4YzQuNDE4LDAsOC0zLjU4Miw4LThzLTMuNTgyLTgtOC04aC04di04YzAtMTcuNjczLDE0LjMyNy0zMiwzMi0zMmMxNy42NzMsMCwzMiwxNC4zMjcsMzIsMzINCgkJCQljMCw0LjQxOCwzLjU4Miw4LDgsOHM4LTMuNTgyLDgtOGMwLTI2LjUxLTIxLjQ5LTQ4LTQ4LTQ4Yy0yNi41MSwwLTQ4LDIxLjQ5LTQ4LDQ4djQwSDE5My43NmMzLjk5NS00LjM3Myw2LjIxOS0xMC4wNzcsNi4yNC0xNg0KCQkJCXYtNDhjMC00LjQxOC0zLjU4Mi04LTgtOEg3MmMtNC40MTgsMC04LDMuNTgyLTgsOHY0OGMwLjAyMSw1LjkyMywyLjI0NSwxMS42MjcsNi4yNCwxNkg4Yy00LjQxOCwwLTgsMy41ODItOCw4djE5Mg0KCQkJCWMwLDQuNDE4LDMuNTgyLDgsOCw4aDQ5NmM0LjQxOCwwLDgtMy41ODIsOC04VjMxMkM1MTIsMzA3LjU4Miw1MDguNDE4LDMwNCw1MDQsMzA0eiBNODAsMjQ4aDEwNHY0MGMwLDQuNDE4LTMuNTgyLDgtOCw4SDg4DQoJCQkJYy00LjQxOCwwLTgtMy41ODItOC04VjI0OHogTTEyOCw0OTZIMTZ2LTk2aDExMlY0OTZ6IE0xMjgsMzg0SDE2di02NGgyNGMwLDQuNDE4LDMuNTgyLDgsOCw4aDQ4YzQuNDE4LDAsOC0zLjU4Miw4LThoMjRWMzg0eg0KCQkJCSBNMzY4LDQ5NkgxNDRWMzIwaDIyNFY0OTZ6IE00OTYsNDk2SDM4NHYtOTZoMTEyVjQ5NnogTTQ5NiwzODRIMzg0di02NGgyNGMwLDQuNDE4LDMuNTgyLDgsOCw4aDQ4YzQuNDE4LDAsOC0zLjU4Miw4LThoMjRWMzg0eiINCgkJCQkvPg0KCQkJPHBhdGggZD0iTTE2MCw0NDBoMTkyYzQuNDE4LDAsOC0zLjU4Miw4LTh2LTgwYzAtNC40MTgtMy41ODItOC04LThIMTYwYy00LjQxOCwwLTgsMy41ODItOCw4djgwQzE1Miw0MzYuNDE4LDE1NS41ODIsNDQwLDE2MCw0NDANCgkJCQl6IE0xNjgsMzYwaDE3NnY2NEgxNjhWMzYweiIvPg0KCQkJPHBhdGggZD0iTTMwNCw0NjRoLTk2Yy00LjQxOCwwLTgsMy41ODItOCw4czMuNTgyLDgsOCw4aDk2YzQuNDE4LDAsOC0zLjU4Miw4LThTMzA4LjQxOCw0NjQsMzA0LDQ2NHoiLz4NCgkJCTxwYXRoIGQ9Ik04OCwxMjBoMzM2YzQuNDE4LDAsOC0zLjU4Miw4LThWOGMwLTQuNDE4LTMuNTgyLTgtOC04SDg4Yy00LjQxOCwwLTgsMy41ODItOCw4djEwNEM4MCwxMTYuNDE4LDgzLjU4MiwxMjAsODgsMTIweg0KCQkJCSBNMjY0LDE2aDE1MnY4OEgyNjRWMTZ6IE05NiwxNmgxNTJ2ODhIOTZWMTZ6Ii8+DQoJCQk8cGF0aCBkPSJNMjAwLDU2aC02NGMtNC40MTgsMC04LDMuNTgyLTgsOHMzLjU4Miw4LDgsOGg2NGM0LjQxOCwwLDgtMy41ODIsOC04UzIwNC40MTgsNTYsMjAwLDU2eiIvPg0KCQkJPHBhdGggZD0iTTM3Niw1NmgtNjRjLTQuNDE4LDAtOCwzLjU4Mi04LDhzMy41ODIsOCw4LDhoNjRjNC40MTgsMCw4LTMuNTgyLDgtOFMzODAuNDE4LDU2LDM3Niw1NnoiLz4NCgkJCTxwYXRoIGQ9Ik05MS4yLDE5Ny42Yy02LjYzMSwzLjgxOC0xMC44NTQsMTAuNzU2LTExLjIsMTguNGMwLDQuNDE4LDMuNTgyLDgsOCw4czgtMy41ODIsOC04YzAtMS42ODgsMC44LTIuNTc2LDQuOC01LjYNCgkJCQljNi42MzEtMy44MTgsMTAuODU0LTEwLjc1NiwxMS4yLTE4LjRjLTAuMzQ2LTcuNjQ0LTQuNTY5LTE0LjU4Mi0xMS4yLTE4LjRjLTQuMDQtMy4wMzItNC44LTMuOTItNC44LTUuNnMwLjgtMi41Niw0LjgtNS42DQoJCQkJYzYuNjMxLTMuODE4LDEwLjg1NC0xMC43NTYsMTEuMi0xOC40YzAtNC40MTgtMy41ODItOC04LThzLTgsMy41ODItOCw4YzAsMS42NzItMC44LDIuNTYtNC44LDUuNg0KCQkJCWMtNi42MzEsMy44MTgtMTAuODU0LDEwLjc1Ni0xMS4yLDE4LjRjMC4zNDYsNy42NDQsNC41NjksMTQuNTgyLDExLjIsMTguNGM0LDMuMDMyLDQuOCwzLjkyLDQuOCw1LjZTOTUuMiwxOTQuNTYsOTEuMiwxOTcuNnoiLz4NCgkJCTxwYXRoIGQ9Ik0xNDAuOCwxNjIuNGM2LjYzMS0zLjgxOCwxMC44NTQtMTAuNzU2LDExLjItMTguNGMwLTQuNDE4LTMuNTgyLTgtOC04cy04LDMuNTgyLTgsOGMwLDEuNjcyLTAuOCwyLjU2LTQuOCw1LjYNCgkJCQljLTYuNjMxLDMuODE4LTEwLjg1NCwxMC43NTYtMTEuMiwxOC40YzAuMzQ2LDcuNjQ0LDQuNTY5LDE0LjU4MiwxMS4yLDE4LjRjNCwzLjAzMiw0LjgsMy45Miw0LjgsNS42cy0wLjgsMi41NzYtNC44LDUuNg0KCQkJCWMtNi42MzEsMy44MTgtMTAuODU0LDEwLjc1Ni0xMS4yLDE4LjRjMCw0LjQxOCwzLjU4Miw4LDgsOHM4LTMuNTgyLDgtOGMwLTEuNjg4LDAuOC0yLjU3Niw0LjgtNS42DQoJCQkJYzYuNjMxLTMuODE4LDEwLjg1NC0xMC43NTYsMTEuMi0xOC40Yy0wLjM0Ni03LjY0NC00LjU2OS0xNC41ODItMTEuMi0xOC40Yy00LTMuMDMyLTQuOC0zLjkyLTQuOC01LjZTMTM2LjgsMTY1LjQxNiwxNDAuOCwxNjIuNA0KCQkJCXoiLz4NCgkJCTxwYXRoIGQ9Ik0xODAuOCwxNjIuNGM2LjYzMS0zLjgxOCwxMC44NTQtMTAuNzU2LDExLjItMTguNGMwLTQuNDE4LTMuNTgyLTgtOC04cy04LDMuNTgyLTgsOGMwLDEuNjcyLTAuOCwyLjU2LTQuOCw1LjYNCgkJCQljLTYuNjMxLDMuODE4LTEwLjg1NCwxMC43NTYtMTEuMiwxOC40YzAuMzQ2LDcuNjQ0LDQuNTY5LDE0LjU4MiwxMS4yLDE4LjRjNCwzLjAzMiw0LjgsMy45Miw0LjgsNS42cy0wLjgsMi41NzYtNC44LDUuNg0KCQkJCWMtNi42MzEsMy44MTgtMTAuODU0LDEwLjc1Ni0xMS4yLDE4LjRjMCw0LjQxOCwzLjU4Miw4LDgsOHM4LTMuNTgyLDgtOGMwLTEuNjg4LDAuOC0yLjU3Niw0LjgtNS42DQoJCQkJYzYuNjMxLTMuODE4LDEwLjg1NC0xMC43NTYsMTEuMi0xOC40Yy0wLjM0Ni03LjY0NC00LjU2OS0xNC41ODItMTEuMi0xOC40Yy00LTMuMDMyLTQuOC0zLjkyLTQuOC01LjZTMTc2LjgsMTY1LjQxNiwxODAuOCwxNjIuNA0KCQkJCXoiLz4NCgkJPC9nPg0KCTwvZz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjwvc3ZnPg0K" />
													<h3>01</h3>
													<span>Kitchen</span>
												</li>
											</ul>
									</div>
									<div class="col-md-6 col-lg-6">
										<img src="images/plan/commercial-a.jpg" alt=""/>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					
				</div>
			</section>
			<section class="content-inner-1">				
				<div class="container">
					<div class="section-head text-center">
						<h2 class="title">USP</h2>
					
					</div>
					<div class="row">
					<div class="team-carousel owl-carousel owl-theme owl-btn-1 m-b30 owl-btn-center-lr">
						<div class="item">
							<div class="team-box style1 text-center wow fadeInUp" data-wow-delay="0.2s">
								<div class="dlab-media"> 
									<a href="#">
										<img style="width:100%; height:250px;" src="images/ups-office.jpg" alt="">
									</a>
								</div>
								<div class="team-info">
									<h5 class="team-title"><a href="#">30 Minutes</a></h5>
									<span class="team-position">to Soetta Airport</span>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="team-box style1 green text-center wow fadeInUp" data-wow-delay="0.4s">
								<div class="dlab-media"> 
									<a href="#">
										<img style="width:100%; height:250px;" src="images/ups-university.png" alt="">
									</a>
								</div>
								<div class="team-info">
									<h5 class="team-title"><a href="#">5 Minutes</a></h5>
									<span class="team-position">to Schools & University</span>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="team-box style1 pink text-center wow fadeInUp" data-wow-delay="0.6s">
								<div class="dlab-media"> 
									<a href="#">
										<img style="width:100%; height:250px;" src="images/ups-office.jpg" alt="">
									</a>
								</div>
								<div class="team-info">
									<h5 class="team-title"><a href="#">5 Minutes</a></h5>
									<span class="team-position">to toll</span>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="team-box style1 yellow text-center wow fadeInUp" data-wow-delay="0.8s">
								<div class="dlab-media"> 
									<a href="#">
										<img style="width:100%; height:250px;" src="images/ups-office.jpg" alt="">
									</a>
								</div>
								<div class="team-info">
									<h5 class="team-title"><a href="#">8 Minutes</a></h5>
									<span class="team-position">to Future MRT Station</span>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="team-box style1 yellow text-center wow fadeInUp" data-wow-delay="0.8s">
								<div class="dlab-media"> 
									<a href="#">
										<img style="width:100%; height:250px;" src="images/ups-mall.png" alt="">
									</a>
								</div>
								<div class="team-info">
									<h5 class="team-title"><a href="#">5 Minutes</a></h5>
									<span class="team-position">to Malls & Entertainment</span>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="team-box style1 yellow text-center wow fadeInUp" data-wow-delay="0.8s">
								<div class="dlab-media"> 
									<a href="#">
										<img style="width:100%; height:250px;" src="images/ups-university.png" alt="">
									</a>
								</div>
								<div class="team-info">
									<h5 class="team-title"><a href="#">1-5 Minutes</a></h5>
									<span class="team-position">to Religious Places</span>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="team-box style1 yellow text-center wow fadeInUp" data-wow-delay="0.8s">
								<div class="dlab-media"> 
									<a href="#">
										<img style="width:100%; height:250px;" src="images/ups-office.jpg" alt="">
									</a>
								</div>
								<div class="team-info">
									<h5 class="team-title"><a href="#">1-5 Minutes</a></h5>
									<span class="team-position">to Hospital</span>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</section>
			<div class="map-view">
				<div class="row spno">
					<div class="col-md-12 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="1s"
					><img id="img-map" style="" src="images/map.jpg" alt=""/></div>
					
				</div>
			</div>
		</div>
		
       <!-- Footer -->
    <footer class="site-footer" id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
					<div class="col-md-6 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.3s">
                        <div class="widget widget_about">
							<div class="footer-logo">
								<a href="index.php"><img src="images/logo.png" alt=""/></a> 
							</div>
							<p>Surround yourself with fresh energy, high-tech amenities, and elevated style. Indulge in extraordinary amenities, relax in appealing social spaces.</p>
						</div>
                    </div>
					<div class="col-md-6 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.6s">
						<div class="widget">
							<h5 class="footer-title">Contact Us</h5>
							<ul class="contact-info-bx">
								<li><i class="las la-map-marker"></i><strong>Address</strong> Tangerang Banten, Indonesia</li>
								<li><i class="las la-phone-volume"></i><strong>Call :-</strong> 0811 1007 078</li>
								<li><i class="las la-phone-square-alt"></i><strong>Board line :-</strong> 0123-4567890</li>
							</ul>
						</div>
                    </div>
                </div>
            </div>
		</div>
</div>
        <!-- footer bottom part -->
        <!-- <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 text-md-left text-center"> <span>© 2021 Cendana Icon. All Right Reserved</span> </div>
                  
                </div>
            </div>
        </div> -->
    </footer>
    <!-- Footer END-->
	<div class="wlcm-form-mobile">
		<div class="enter-form" style="width:100%;">
			<a style="float:right; z-index:99999;" href="#" id="close-popup-form">
				<i class="las la-times"></i>
			</a>
			
		</div>
    </div>

	<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content  enter-form">
      <div class="modal-header">
        <button style="color:white;" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
      </div>
      <div class="modal-body">
	  		<form action="index2.php">
				<h2 class="wlcm-form-title">Dapatkan infromasi terkini</h2>
				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="las la-user"></span>
					</div>
					<input type="text" class="form-control" placeholder="Nama" aria-label="Your Name" required/>
				</div>
				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="las la-envelope"></span>
					</div>
					<input type="email" class="form-control" placeholder="Email" aria-label="Your Name" required/>
				</div>
				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="las las la-tty"></span>
					</div>
					<input type="text" class="form-control" placeholder="Phone No." aria-label="Phone No." required/>
				</div>
				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="la la-money"></span>
					</div>
					<select class="form-control">
						<option>Harga Property</option>
						<option value="1">< 500 Juta</option>
						<option value="2">500 Juta -  1 Milyar</option>
						<option value="3">1 Milyar - 1,5 Milyar</option>
					</select>
				</div>
				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="la la-home"></span>
					</div>
					<select class="form-control">
						<option>Tipe Property</option>
						<option value="1">Cendana Villa</option>
						<option value="2">Cendana Residence</option>
						<option value="3">Cendana COTTAGE</option>
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
      </div>
    </div>
  </div>
</div>

	


<!-- JAVASCRIPT FILES ========================================= -->
<script src="js/jquery.min.js"></script><!-- JQUERY.MIN JS -->
<script src="vendor/wow/wow.js"></script><!-- WOW JS -->
<script src="vendor/bootstrap/js/popper.min.js"></script><!-- POPPER.MIN JS -->
<!-- <script src="vendor/bootstrap/js/bootstrap.min.js"></script>BOOTSTRAP.MIN JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script src="vendor/owl-carousel/owl.carousel.js"></script><!-- OWL-CAROUSEL JS -->
<script src="vendor/magnific-popup/magnific-popup.js"></script><!-- MAGNIFIC-POPUP JS -->
<script src="vendor/counter/waypoints-min.js"></script><!-- WAYPOINTS JS -->
<script src="vendor/counter/counterup.min.js"></script><!-- COUNTERUP JS -->
<script src="vendor/imagesloaded/imagesloaded.js"></script><!-- IMAGESLOADED -->
<script src="vendor/masonry/masonry.js"></script>
<script src="vendor/masonry/masonry.filter.js"></script>
<script src="vendor/lightgallery/js/lightgallery-all.min.js"></script><!-- LIGHTGALLERY -->
<script src="js/dz.carousel.js"></script><!-- OWL-CAROUSEL -->
<script src="js/picker.min.js"></script><!-- JQUERY.MIN JS -->
<script src="js/custom.js?v=<?=date('YmdHis');?>"></script><!-- CUSTOM JS -->


<script>
jQuery(document).ready(function() {
	jQuery("#btn-hub-kami-mobile").click(function(e){
		e.preventDefault();
		jQuery(".wlcm-form-mobile").animate({bottom: '50px'},'slow');
	}); 

	jQuery("#close-popup-form").click(function(e){
		e.preventDefault();
		console.log('test');
		jQuery(".wlcm-form-mobile").hide();
	}); 

	'use strict';

	var galleryTop = $("#galleryTop");
  var galleryBottom = $("#galleryBottom");
  var slidesPerPage = 4; //globaly define number of elements per page
  var syncedSecondary = true;

	  galleryTop.owlCarousel({
		items : 2,
		autoplaySpeed: 3000,
		navSpeed: 3000,
		paginationSpeed: 3000,
		slideSpeed: 3000,
		smartSpeed: 3000,
        autoplay: 3000,
		nav: false,
		center:true,
		autoWidth:true,
		dots: false,
		loop: true,
		responsiveRefreshRate : 200,
		responsive:{
			0:{
				items : 1,
				autoWidth:false,
				stagePadding:30
			},
			767:{
				autoWidth:false
			},
			1200:{
				items : 2
			}
		}	
	  })
	  
	  .on('changed.owl.carousel', syncPosition);

	  galleryBottom.on('initialized.owl.carousel', function () {
		  galleryBottom.find(".owl-item").eq(0).addClass("current");
		}).owlCarousel({
		items : slidesPerPage,
		dots: false,
		nav: false,
		margin:20,
		autoplaySpeed: 3000,
		navSpeed: 3000,
		paginationSpeed: 3000,
		slideSpeed: 3000,
		smartSpeed: 3000,
        autoplay: 3000,
		stagePadding:100,
		slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
		responsiveRefreshRate : 50,
		navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
		responsive:{
			0:{
				items:2,
				stagePadding:30,
				margin:15
			},
			480:{
				items:2
			},			
			768:{
				items:3
			},
			1024:{
				items:4
			},
			1400:{
				items:5
			}
		}
	  }).on('changed.owl.carousel', syncPosition2);

  function syncPosition(el) {
    //if you set loop to false, you have to restore this next line
    //var current = el.item.index;
    
    //if you disable loop you have to comment this block
    var count = el.item.count-1;
    var current = Math.round(el.item.index - (el.item.count/2) - .5);
    
    if(current < 0) {
      current = count;
    }
    if(current > count) {
      current = 0;
    }
    
    //end block

    galleryBottom
      .find(".owl-item")
      .removeClass("current")
      .eq(current)
      .addClass("current");
    var onscreen = galleryBottom.find('.owl-item.active').length - 1;
    var start = galleryBottom.find('.owl-item.active').first().index();
    var end = galleryBottom.find('.owl-item.active').last().index();
    
    if (current > end) {
      galleryBottom.data('owl.carousel').to(current, 100, true);
    }
    if (current < start) {
      galleryBottom.data('owl.carousel').to(current - onscreen, 100, true);
    }
  }
  
  function syncPosition2(el) {
    if(syncedSecondary) {
      var number = el.item.index;
      galleryTop.data('owl.carousel').to(number, 100, true);
    }
  }
  
  galleryBottom.on("click", ".owl-item", function(e){
		e.preventDefault();
		var number = $(this).index();
		//galleryTop.data('owl.carousel').to(number, 300, true);
		
		galleryTop.data('owl.carousel').to(number, 300, true);
		
	});
});	/*ready*/
</script>
</body>
</html>